// server.js
const express = require('express');
const stripe = require('stripe')('scret key');
const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/public/index.html');
});

app.post('/charge', (req, res) => {
  const amount = 2000; // Amount in cents (e.g., $20.00)
  stripe.charges.create(
    {
      amount,
      currency: 'usd',
      source: req.body.stripeToken, // Token created by the Stripe.js script
    },
    (err, charge) => {
      if (err) {
        console.error(err);
        res.send('Payment failed');
      } else {
        res.send('Payment successful');
      }
    }
  );
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
